<?php

namespace Thing;

/**
 * Klasa pudełka (Adaptera, Obserwatora lub Komponentu) pozwalająca na korzystanie
 * i ujednolicenia dostępu do danych z klasy OldBox.
 * Wykorzystana do przedstawienia wzorca: Adapter, Obserwator, Strategia
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Pack
 * @package   Thing
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class Box implements Pack
{
	/**
	 * Stare pudełko
	 * 
	 * @access private
	 * @var    OldBox $_oldbox
	 */
	private $_oldbox = null;
	
	/**
	 * Konstruktor ustawiający wagę pudełka
	 * 
	 * @access public
	 * @param  float $weight
	 */
	public function __construct($weight)
	{
		$this->_oldbox = new OldBox($weight);
	}
	
	/**
	 * Pobiera wagę pudełka
	 * 
	 * @access public
	 * @return float
	 */
	public function weight()
	{
		return $this->_oldbox->boxWeight();
	}
	
	/**
	 * Niszy pudełko w momencie zdarzenia
	 * 
	 * @access public
	 * @return Box
	 */
	public function crash()
	{
		echo 'Box destroy!' . "\n";
		
		return $this;
	}
}