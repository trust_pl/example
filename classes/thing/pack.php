<?php

namespace Thing;

use Base\Weight;

/**
 * Intefejs (Obserwatora lub Liścia) do obsługi zniżczenia paczki
 * w momęcie zdarzenia. Wykorzystany do przedstawienia 
 * wzorca: Obserwator, Kompozyt
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Pack
 * @package   Thing	
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 */
interface Pack extends Weight
{
	/**
	 * Niszy paczkę w momencie zdarzenia
	 * 
	 * @access public
	 * @return Pack
	 */
	public function crash();
}