<?php

namespace Thing;

/**
 * Klasa walizki (Obserwatora lub Komponentu) ładowanej
 * do bagażnika lub na przyczepę pojazdu
 * Wykorzystana do przedstawienia wzorca: Obserwator, Strategia
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Pack
 * @package   Thing
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class Suitcase implements Pack
{
	/**
	 * Waga walizki
	 * 
	 * @access private
	 * @var    float
	 */
	private $_weight = null;
	
	/**
	 * Konstruktor ustawiający wagę walizki
	 * 
	 * @access public
	 * @param  float $weight
	 */
	public function __construct($weight)
	{
		$this->_weight = $weight;
	}
	
	/**
	 * Pobiera wagę walizki
	 * 
	 * @access public
	 * @return float
	 */
	public function weight()
	{
		return $this->_weight;
	}
	
	/**
	 * Niszy walizkę w momencie zdarzenia
	 * 
	 * @access public
	 * @return Suitcase
	 */
	public function crash()
	{
		echo 'Suitcase destroy!' . "\n";
		
		return $this;
	}
}