<?php

namespace Thing;

/**
 * Klasa starego pudełka (Adoptowana) użyta w celu wykorzystania
 * jej metody niekompatybilnej z pozostałymi obiektami tego typu.
 * Wykożystany do przedstawienia wzorca: Adapter
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Pack
 * @package   Thing
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class OldBox
{
	/**
	 * Waga pudełka
	 * 
	 * @access private
	 * @var    float
	 */
	private $_boxWeight = null;
	
	/**
	 * Konstruktor ustawiający wagę pudełka
	 * 
	 * @access public
	 * @param  float $weight
	 */
	public function __construct($weight)
	{
		$this->_boxWeight = (float) $weight;
	}
	
	/**
	 * Pobiera wagę pudełka
	 * 
	 * @access public
	 * @return float
	 */
	public function boxWeight()
	{
		return $this->_boxWeight;
	}
}