<?php

namespace Vehicle;

use Base\Data;
use Base\Weight;
use Thing\Pack;

/**
 * Klasa bagażnika (Kompozytu, Kontekstu lub Obserwowany) przyjmującego
 * paczki. Wykorzystany do przedstawienia wzorca: 
 * Strategia, Obserwator, Kompozyt
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Car
 * @package   Vehicle
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class Hold extends Data implements Weight
{
	/**
	 * Ładowność
	 * 
	 * @access private
	 * @var    float
	 */
	private $_payload = null;
	
	/**
	 * Konstruktor ustawiający ładowność
	 * 
	 * @access public
	 * @param  float $payload
	 */
	public function __construct($payload)
	{
		$this->_payload = $payload;
	}
	
	/**
	 * Pobiera ładowność pojazdu
	 * 
	 * @access public
	 * @return float
	 */
	public function payload()
	{
		return $this->_payload;
	}
	
	/**
	 * Wagę załadunku
	 * 
	 * @access public
	 * @return float
	 */
	public function weight()
	{
		$weight = 0;
		
		foreach($this as $object)
		{
			$weight += $object->weight();
		}
		
		return $weight;
	}
	
	/**
	 * Pobiera miejsce do załadowania
	 * 
	 * @access public
	 * @return float
	 */
	public function toLoad()
	{
		return $this->payload() - $this->weight();
	}
	
	/**
	 * Ładuje paczkę
	 * 
	 * @access public
	 * @param  Pack $object
	 * @return bool
	 */
	public function load(Pack $object)
	{
		if($this->toLoad() >= $object->weight())
		{
			$this[spl_object_hash($object)] = $object;
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Wywołuje zdarzenie
	 * 
	 * @access public
	 * @return Hold
	 */
	public function event()
	{
		foreach($this as $object)
		{
			$object->crash();
		}
		
		return $this;
	}
	
	// Obserwator
	public function attach(SplObserver $object)
	{
		$this->load($object);
		
		return $this;
	}
	
	// Obserwator
	public function detach(SplObserver $object)
	{
		unset($this[spl_object_hash($object)]);
		
		return $this;
	}
	
	// Obserwator
	public function notify()
	{
		$this->event();
		
		return $this;
	}
}