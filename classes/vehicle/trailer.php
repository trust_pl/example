<?php

namespace Vehicle;

/**
 * Klasa przyczepy (Kompozytu, Kontekstu lub Obserwowany) przyjmującej
 * paczki. Wykorzystany do przedstawienia wzorca: 
 * Strategia, Obserwator, Kompozyt
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Car
 * @package   Vehicle
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class Trailer extends Hold
{
	
}