<?php

namespace Vehicle;

use Vehicle\Car\Truck;

/**
 * Ustawia cechę pojazdu pozwala rozszerzyć go o rodzaj 
 * napędu. Wykorzystany do przedstawienia wzorca: 
 * Template method
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Element
 * @package   Vehicle
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 */
trait Drive
{
	/**
	 * Pobiera na ile kół jest napęd pojazdu
	 * 
	 * @final
	 * @access public
	 * @return int
	 */
	public final function drive()
	{
		if($this instanceof Truck)
		{
			return 8;
		}
		
		return 2;
	}
}