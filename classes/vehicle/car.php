<?php

namespace Vehicle;

use Base\Weight;
use Thing\Pack;

/**
 * Klasa samochodu bazowego (Fasada) implementująca metody elemntów
 * składowych. Wykorzystany do przedstawienia wzorca: 
 * Fasada, Fabryka, Template method, Wstrzykiwanie zależności
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Car
 * @package   Vehicle
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @abstract
 */
abstract class Car implements Weight
{
	use Drive;
	
	/**
	 * rodzaj pojazdu
	 * 
	 * @access protected
	 * @var    string
	 */
	protected $_type = null;
	
	/**
	 * Waga własna pojazdu
	 * 
	 * @access protected
	 * @var    float
	 */
	protected $_weight = null;
	
	/**
	 * Silnik pojazdu
	 * 
	 * @access protected
	 * @var    Engine
	 */
	protected $_engine = null;
	
	/**
	 * Bagażnik pojazdu
	 * 
	 * @access protected
	 * @var    Hold
	 */
	protected $_hold = null;
	
	/**
	 * Fabryka pojazdów
	 * 
	 * @access public
	 * @param  float $weight
	 * @param  Engine $engine
	 * @param  Hold $hold
	 * @return Car
	 */
	public static function factony($name, $weight = null, Engine $engine = null, Hold $hold = null)
	{
		$class = 'Vehicle\\Car\\' . ucfirst($name);
		
		return new $class($weight, $engine, $hold);
	}
	
	/**
	 * Konstruktor ustawiający wagę, silnik i bagażnik
	 * 
	 * @access public
	 * @param  float $weight
	 * @param  Engine $engine
	 * @param  Hold $hold
	 */
	public function __construct($weight = null, Engine $engine = null, Hold $hold = null)
	{
		$this->_type = get_class($this);
		
		if(empty($weight))
		{
			$weight = 1100;
		}
		
		if(empty($engine))
		{
			$engine = new Engine(1400, 80);
		}
		
		if(empty($hold))
		{
			$hold = new Hold(400);
		}
		
		$this->_weight = $weight;
		$this->_engine = $engine;
		$this->_hold = $hold;
	}
	
	/**
	 * Pobiera rodzaj pojazdu
	 * 
	 * @final
	 * @access public
	 * @return float
	 */
	public final function type()
	{
		return $this->_type;
	}
	
	/**
	 * Pobiera moc pojazdu
	 * 
	 * @access public
	 * @return float
	 */
	public function capacity()
	{
		return $this->_engine->capacity();
	}
	
	/**
	 * Pobiera moc silnikai
	 * 
	 * @access public
	 * @return float
	 */
	public function power()
	{
		return $this->_engine->power();
	}
	
	/**
	 * Pobiera wagę pojazdu z bagażem
	 * 
	 * @access public
	 * @return float
	 */
	public function weight()
	{
		return $this->_weight + $this->_engine->weight() + $this->_hold->weight();
	}
	
	/**
	 * Pobiera ładowność pojazdu
	 * 
	 * @access public
	 * @return float
	 */
	public function payload()
	{
		return $this->_hold->payload();
	}
	
	/**
	 * Pobiera miejsce do załadowania pojazdu
	 * 
	 * @access public
	 * @return float
	 */
	public function toLoad()
	{
		return $this->_hold->toLoad();
	}
	
	/**
	 * Ładuje paczkę na pojazd
	 * 
	 * @access public
	 * @param  Pack $object
	 * @return bool
	 */
	public function load(Pack $object)
	{
		return $this->_hold->load($object);
	}
	
	/**
	 * Sprawdza czy pojazd ma przyczepę
	 * 
	 * @access public
	 * @return bool
	 */
	public function hasTrailer()
	{
		return false;
	}
	
	/**
	 * Wywołuje zdarzenie na pojeździe
	 * 
	 * @access public
	 * @return Car
	 */
	public function event()
	{
		echo 'Car destroy!' . "\n";
		
		$this->_hold->event();
		
		return $this;
	}
}