<?php

namespace Vehicle\Car;

use Vehicle\Car;
use Vehicle\Engine;
use Vehicle\Hook;
use Vehicle\Hold;
use Vehicle\Trailer;
use Thing\Pack;

/**
 * Klasa samochodu ciężarowego (Fasada lub Dekoratora) z możliwością dopięcia
 * przyczepy. Wykorzystany do przedstawienia wzorca: 
 * Dekorator, Wstrzykiwanie zależności
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Car
 * @package   Vehicle
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class Truck extends Car implements Hook
{
	/**
	 * Przyczepa pojazdu
	 * 
	 * @access protected
	 * @var    Trailer
	 */
	protected $_trailer = null;
	
	/**
	 * Konstruktor ustawiający wagę, silnik i bagażnik
	 * 
	 * @access public
	 * @param  float $weight
	 * @param  Engine $engine
	 * @param  Hold $hold
	 */
	public function __construct($weight = null, Engine $engine = null, Hold $hold = null)
	{
		if(empty($weight))
		{
			$weight = 3600;
		}
		
		if(empty($engine))
		{
			$engine = new Engine(4500, 210);
		}
		
		if(empty($hold))
		{
			$hold = new Hold(2000);
		}
		
		parent::__construct($weight, $engine, $hold);
	}
	
	/**
	 * Ustawia przyczepę pojazdu
	 * 
	 * @access public
	 * @param  Trailer $trailer
	 * @return Truck
	 */
	public function hookUp(Trailer $trailer)
	{
		$this->_trailer = $trailer;
		
		return $this;
	}
	
	/**
	 * Pobiera wagę pojazdu z bagażem
	 * 
	 * @access public
	 * @return float
	 */
	public function weight()
	{
		if($this->hasTrailer())
		{
			return parent::weight() + $this->_trailer->weight();
		}
		
		return parent::weight();
	}
	
	/**
	 * Pobiera ładowność pojazdu
	 * 
	 * @access public
	 * @return float
	 */
	public function payload()
	{
		if($this->hasTrailer())
		{
			return parent::payload() + $this->_trailer->payload();
		}
		
		return parent::payload();
	}
	
	/**
	 * Pobiera miejsce do załadowania pojazdu
	 * 
	 * @access public
	 * @return float
	 */
	public function toLoad()
	{
		if($this->hasTrailer())
		{
			return parent::toLoad() + $this->_trailer->toLoad();
		}
		
		return parent::toLoad();
	}
	
	/**
	 * Ładuje paczkę na pojazd
	 * 
	 * @access public
	 * @param  Pack $object
	 * @return bool
	 */
	public function load(Pack $object)
	{
		return parent::load($object) || ($this->hasTrailer() && $this->_trailer->load($object));
	}
	
	/**
	 * Sprawdza czy pojazd ma przyczepę
	 * 
	 * @access public
	 * @return bool
	 */
	public function hasTrailer()
	{
		return isset($this->_trailer);
	}
	
	/**
	 * Wywołuje zdarzenie na pojeździe
	 * 
	 * @access public
	 * @return Truck
	 */
	public function event()
	{
		parent::event();
		
		if($this->hasTrailer())
		{
			$this->_trailer->event();
		}
		
		return $this;
	}
}