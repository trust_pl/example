<?php

namespace Vehicle\Car;

use Vehicle\Car;

/**
 * Klasa samochodu osobowego (Fasada) implementująca metody elemntów
 * składowych. Wykorzystany do przedstawienia wzorca: 
 * Fasada, Fabryka, Template method, Wstrzykiwanie zależności
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Car
 * @package   Vehicle
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class Personal extends Car
{
	
}