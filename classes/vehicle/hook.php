<?php

namespace Vehicle;

use Base\Weight;

/**
 * Intefejs (Dekoratora) do obsługi przyczepy
 * pojazdu. Wykorzystane do przedstawienia wzorca: 
 * Dekorator
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Car
 * @package   Vehicle
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 */
interface Hook extends Weight
{
	/**
	 * Ustawia przyczepę pojazdu
	 * 
	 * @access public
	 * @param  Trailer $trailer
	 * @return Hook
	 */
	public function hookUp(Trailer $trailer);
}