<?php

namespace Vehicle;

use Base\Weight;

/**
 * Klasa silnika pajazdu okrślącego jego moc i pojemność.
 * Wykorzystany do przedstawienia wzorca: 
 * Fasada, Wstrzykiwanie zależności
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Element
 * @package   Vehicle
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class Engine implements Weight
{
	/**
	 * Pojemność silnika
	 * 
	 * @access private
	 * @var    float
	 */
	private $_capacity = null;
	
	/**
	 * Moc silnikai
	 * 
	 * @access private
	 * @var    float
	 */
	private $_power = null;
	
	/**
	 * Konstruktor ustawiający moc i pojemność silnika
	 * 
	 * @access public
	 * @param  float $capacity
	 * @param  float $power
	 */
	public function __construct($capacity, $power)
	{
		$this->_capacity = $capacity;
		$this->_power = $power;
	}
	
	/**
	 * Pobiera pojemność silnika
	 * 
	 * @access public
	 * @return float
	 */
	public function capacity()
	{
		return $this->_capacity;
	}
	
	/**
	 * Pobiera moc silnikai
	 * 
	 * @access public
	 * @return float
	 */
	public function power()
	{
		return $this->_power;
	}
	
	/**
	 * Pobiera wagę silnika wynikającą z jego pojemności i mocy
	 * 
	 * @access public
	 * @return float
	 */
	public function weight()
	{
		return round($this->_capacity / 10 + $this->_power / 10);
	}
}