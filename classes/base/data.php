<?php

namespace Base;

/**
 * Klasa implementująca interfejs ArrayData do obsługi danych.
 * Rozwija działanie wszystkich funkcj interfejsu
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Data
 * @package   Base
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @abstract
 */
abstract class Data implements ArrayData
{
	/**
	 * Dane obiektu
	 * 
	 * @access protected
	 * @var    array
	 */
	protected $_data = array();
	
	/**
	 * Rzutuje obiekt na tablicę
	 * 
	 * @access public
	 * @return array
	 */
	public function asArray()
	{
		return $this->_data;
	}
	
	/**
	 * Pobiera daną obiektu
	 * 
	 * @access public
	 * @param  string $name
	 * @param  mixed $default
	 * @return mixed
	 */
	public function get($name, $default = null)
	{
		if(isset($this->_data[$name]))
		{
			return $this->_data[$name];
		}
		
		return $default;
	}
	
	/**
	 * Ustawia daną obiektu
	 * 
	 * @access public
	 * @param  string $name
	 * @param  mixed $value
	 * @return Data
	 */
	public function set($name, $value)
	{
		if($name === null)
		{
			$this->_data[] = $value;
		}
		else
		{
			$this->_data[$name] = $value;
		}
		
		return $this;
	}
	
	/**
	 * Pobiera daną obiektu
	 * 
	 * @access public
	 * @param  string $name
	 * @return mixed
	 */
	public function __get($name)
	{
		return $this->get($name);
	}
	
	/**
	 * Ustawia daną obiektu
	 * 
	 * @access public
	 * @param  string $name
	 * @param  mixed $value
	 */
	public function __set($name, $value)
	{
		$this->set($name, $value);
	}
	
	/**
	 * Sprawdza czy dana istnieje w obiekcie
	 * 
	 * @access public
	 * @param  string $name
	 * @return bool
	 */
	public function __isset($name)
	{
		return isset($this->_data[$name]);
	}
	
	/**
	 * Usuwa daną obiektu
	 * 
	 * @access public
	 * @param  string $name
	 */
	public function __unset($name)
	{
		unset($this->_data[$name]);
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * @access public
	 * @see    Countable::count()
	 */
	public function count()
	{
		return count($this->_data);
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * @access public
	 * @see    SeekableIterator::seek()
	 */
	public function seek($name)
	{
		if($this->__isset($name))
		{
			$this->rewind();
			
			foreach($this as $key => $value)
			{
				if($name === $key)
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * (non-PHPdoc)
	 * 
	 * @access public
	 * @see    Iterator::current()
	 */
	public function current()
	{
		return current($this->_data);
	}
	
	/**
	 * (non-PHPdoc)
	 * 
	 * @access public
	 * @see    Iterator::key()
	 */
	public function key()
	{
		return key($this->_data);
	}
	
	/**
	 * (non-PHPdoc)
	 * 
	 * @access public
	 * @see    Iterator::valid()
	 */
	public function valid()
	{
		return $this->__isset($this->key());
	}
	
	/**
	 * (non-PHPdoc)
	 * 
	 * @access public
	 * @see    Iterator::rewind()
	 */
	public function rewind()
	{
		reset($this->_data);
		
		return $this;
	}
	
	/**
	 * (non-PHPdoc)
	 * 
	 * @access public
	 * @see    Iterator::next()
	 */
	public function next()
	{
		next($this->_data);
		
		return $this;
	}
	
	/**
	 * Przechodzi do poprzedniej pozycji
	 * 
	 * @access public
	 * @return Data
	 */
	public function prev()
	{
		prev($this->_data);
		
		return $this;
	}
	
	/**
	 * (non-PHPdoc)
	 * 
	 * @access public
	 * @see    ArrayAccess::offsetGet()
	 */
	public function offsetGet($name)
	{
		return $this->get($name);
	}
	
	/**
	 * (non-PHPdoc)
	 * 
	 * @access public
	 * @see    ArrayAccess::offsetSet()
	 */
	public function offsetSet($name, $value)
	{
		$this->set($name, $value);
	}
	
	/**
	 * (non-PHPdoc)
	 * 
	 * @access public
	 * @see    ArrayAccess::offsetExists()
	 */
	public function offsetExists($name)
	{
		return $this->__isset($name);
	}
	
	/**
	 * (non-PHPdoc)
	 * 
	 * @access public
	 * @see    ArrayAccess::offsetUnset()
	 */
	public function offsetUnset($name)
	{
		$this->__unset($name);
	}
}