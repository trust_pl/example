<?php

namespace Base;

/**
 * Intefejs (Komponentu lub Strategii) do obsługi 
 * wagi, wymusza w obiekcie zaimplementowania funkci do 
 * pobieranie jej wartości. Wykorzystany do przedstawienia 
 * wzorca: Dekorator, Strategia, Kompozyt
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Weight
 * @package   Base
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 */
interface Weight
{
	/**
	 * Pobiera wagę obiektu
	 * 
	 * @access public
	 * @return float
	 */
	public function weight();
}