<?php

namespace Base;

/**
 * Interfejs do obługi dancyh obiektu wymuszający
 * ustawienie funkcji magicznych oraz seterów i geterów
 * 
 * @author    Adam Mrowiec <mrowiec.adam@gmail.com>
 * @copyright Copyright © 2016 exasoft
 * @version   1.0.0
 * @category  Data
 * @package   Base
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 */
interface ArrayData extends \Countable, \SeekableIterator, \ArrayAccess
{
	/**
	 * Rzutuje obiekt na tablicę
	 * 
	 * @access public
	 * @return array
	 */
	public function asArray();
	
	/**
	 * Pobiera daną obiektu
	 * 
	 * @access public
	 * @param  string $name
	 * @param  mixed $default
	 * @return mixed
	 */
	public function get($name, $default = null);
	
	/**
	 * Ustawia daną obiektu
	 * 
	 * @access public
	 * @param  string $name
	 * @param  mixed $value
	 * @return ArrayData
	 */
	public function set($name, $value);
	
	/**
	 * Pobiera daną obiektu
	 * 
	 * @access public
	 * @param  string $name
	 * @return mixed
	 */
	public function __get($name);
	
	/**
	 * Ustawia daną obiektu
	 * 
	 * @access public
	 * @param  string $name
	 * @param  mixed $value
	 */
	public function __set($name, $value);
	
	/**
	 * Sprawdza czy dana istnieje w obiekcie
	 * 
	 * @access public
	 * @param  string $name
	 * @return bool
	 */
	public function __isset($name);
	
	/**
	 * Usuwa daną obiektu
	 * 
	 * @access public
	 * @param  string $name
	 */
	public function __unset($name);
}