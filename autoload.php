<?php

define('DOCROOT', realpath(substr(__FILE__, 0, strlen(__FILE__) - strlen(basename(__FILE__)))) . DIRECTORY_SEPARATOR);
define('EXT', '.php');

spl_autoload_register(function($class){
	
	$file = DOCROOT . 'classes' . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, strtolower($class)) . EXT;
	
	if(file_exists($file))
	{
		require $file;
		
		return true;
	}
	
	return false;
});