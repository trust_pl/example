<?php

include_once 'autoload.php';

use Vehicle\Car;
use Vehicle\Trailer;
use Thing\Suitcase;
use Thing\Box;

Car::factony('truck');

$cars = array();

$cars[] = Car::factony('personal');
$cars[] = Car::factony('truck');
$cars[] = Car::factony('truck');

$cars[0]->load(new Suitcase(40));
$cars[1]->load(new Suitcase(40));
$cars[1]->load(new Box(60));
$cars[2]->hookUp(new Trailer(2000));

$cars[0]->event();

$car = Car::factony('personal');

$car->load(new Suitcase(30));
$car->load(new Suitcase(30));
$car->load(new Suitcase(30));
$car->load(new Suitcase(30));

echo $car->weight() . "\n";

echo $car->toLoad() . "\n";

$car->load(new Box(80));

echo $car->weight() . "\n";

echo $car->toLoad() . "\n";

$car->load(new Box(200));

echo $car->weight() . "\n";

echo $car->toLoad() . "\n";

foreach($cars as $car)
{
	echo 'Type: ' . $car->type() . "\n";
	echo 'Capacity: ' . $car->capacity() . "\n";
	echo 'Power: ' . $car->power() . "\n";
	echo 'Drive: ' . $car->drive() . "\n";
	echo 'Weight: ' . $car->weight() . "\n";
	echo 'Payload: ' . $car->payload() . "\n";
	echo 'To load: ' . $car->toLoad() . "\n";
}

var_dump($cars);