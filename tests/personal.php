<?php

use Vehicle\Car;
use Vehicle\Drive;
use Thing\Suitcase;
use Thing\Box;

class TestPersonal extends PHPUnit_Framework_TestCase
{
	public function testInterface()
	{
		$car = Car::factony('personal');
		
		$this->assertInstanceOf('Base\Weight', $car);
	}
	
	public function testClass()
	{
		$car = Car::factony('personal');
		
		$this->assertInstanceOf('Vehicle\Car', $car);
		$this->assertInstanceOf('Vehicle\Car\Personal', $car);
	}
	
	public function testType()
	{
		$car = Car::factony('personal');
		
		$this->assertEquals('Vehicle\Car\Personal', $car->type());
	}
	
	public function testCapacity()
	{
		$car = Car::factony('personal');
		
		$this->assertEquals(1400, $car->capacity());
	}
	
	public function testPower()
	{
		$car = Car::factony('personal');
		
		$this->assertEquals(80, $car->power());
	}
	
	public function testDrive()
	{
		$car = Car::factony('personal');
		
		$this->assertEquals(20, $car->drive());
	}
	
	public function testWeight()
	{
		$car = Car::factony('personal');
		
		$this->assertEquals(1248, $car->weight());
	}
	
	public function testPayload()
	{
		$car = Car::factony('personal');
		
		$this->assertEquals(400, $car->payload());
	}
	
	public function testToLoad()
	{
		$car = Car::factony('personal');
		
		$this->assertEquals($car->payload(), $car->toLoad());
	}
	
	public function testToLoad()
	{
		$car = Car::factony('personal');
		
		$this->assertFalse($car->hasTrailer());
	}
	
	public function testLoad()
	{
		$car = Car::factony('personal');
		
		$car->load(new Suitcase(30));
		$car->load(new Suitcase(30));
		$car->load(new Suitcase(30));
		$car->load(new Suitcase(30));
		
		$this->assertEquals(1368, $car->weight());
		
		$this->assertEquals(280, $car->toLoad());
		
		$car->load(new Box(80));
		
		$this->assertEquals(1448, $car->weight());
		
		$this->assertEquals(200, $car->toLoad());
		
		$car->load(new Box(200));
		
		$this->assertEquals(1648, $car->weight());
		
		$this->assertEquals(0, $car->toLoad());
	}
}